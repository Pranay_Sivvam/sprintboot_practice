package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Student {
	
	@Id@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int studentId;
	
	@Column(name="studentName")
	private String studentName;
	private double fee;
	
	public Student() {
		super();
	}

	public Student(int studentId, String studentName, double fee) {
		super();
		this.studentId = studentId;
		this.studentName = studentName;
		this.fee = fee;
	}
	
	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public double getFee() {
		return fee;
	}

	public void setFee(double fee) {
		this.fee = fee;
	}

	@Override
	public String toString() {
		return "Student [studentId=" + studentId + ", studentName=" + studentName + ", fee=" + fee + "]";
	}

	

}

	
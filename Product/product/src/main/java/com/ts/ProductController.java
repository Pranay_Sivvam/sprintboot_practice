package com.ts;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.ProductDAO;
import com.model.Product;

@RestController
public class ProductController {
	
	//Dependency Injection
		@Autowired
		ProductDAO prodDAO;
		
		@GetMapping("getProducts")
		public List<Product> getProducts() {
			List<Product> prodList = prodDAO.getProducts();
			return prodList;
		}

		@GetMapping("getProductById/{id}")
		public Product getProductById(@PathVariable("id") int prodId) {
			Product product = prodDAO.getProductById(prodId);
			return product;
		}

		@GetMapping("getProductByName/{name}")
		public Product getProductByName(@PathVariable("name") String prodName) {
			return prodDAO.getProductByName(prodName);
		}

		@PostMapping("addProduct")
		public Product addProduct(@RequestBody Product product) {
			return prodDAO.addProduct(product);
		}
		
		@PutMapping("updateProduct")
		public Product updateProduct(@RequestBody Product product) {
			return prodDAO.updateProduct(product);
		}
		
		@DeleteMapping("deleteProductById/{id}")
		public String deleteProductById(@PathVariable("id") int prodId) {
			prodDAO.deleteProductById(prodId);
			return "Product with ProductId: " + prodId + ", Deleted Successfully";
		}
		
		

	@GetMapping("getProduct")
	public Product getProduct() {		
		Product product = new Product(); 		
		product.setProductId(101);
		product.setProductName("Laptop");
		product.setPrice(45000.00);		
		return product;
	}
	
	@GetMapping("getAllProducts")
	public List<Product> getAllProducts() {
		
		List<Product> productList = new ArrayList<Product>();
		
		Product product1 = new Product(101, "Laptop", 54999.00);
		Product product2 = new Product(102, "Mobile", 98999.00); 
		Product product3 = new Product(103, "Pendrive", 599.00); 
	
		productList.add(product1);
		productList.add(product2);
		productList.add(product3);
		
		return productList;
	}
	
	
	
}
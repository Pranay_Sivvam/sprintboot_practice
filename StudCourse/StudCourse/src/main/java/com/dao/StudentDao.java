package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Student;

@Service
public class StudentDao {

	@Autowired
	StudentRepository studentRepo;

	public List<Student> getStudents() {
		// TODO Auto-generated method stub
		return studentRepo.findAll();
	}

	

	public Student getStudentByName(String stdName) {
		// TODO Auto-generated method stub
		return studentRepo.findByName(stdName);
	}

	public Student addStudent(Student student) {
		
		return studentRepo.save(student);
	}

	public Student updateStudent(Student student) {
		// TODO Auto-generated method stub
		return studentRepo.save(student);
	}

	public void deleteStudentById(int stdId) {
		studentRepo.deleteById(stdId);
		
	}

	public Student getLogin(String emailId, String password) {
		
		return studentRepo.getLogin(emailId,password);
	}



	public Student getStudentById(int stdId) {
		
		return studentRepo.findById(stdId).orElse(null);
	}

	
}
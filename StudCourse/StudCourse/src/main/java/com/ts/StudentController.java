package com.ts;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.StudentDao;
import com.model.Student;


@RestController
public class StudentController {
	@Autowired
	StudentDao studentDao;
	
	@GetMapping("getStudents")
	public List<Student> getStudents() {
		return studentDao.getStudents();
	}
	
	@GetMapping("getStudentById/{id}")
	public Student getStudentById(@PathVariable("id") int stdId) {
		return studentDao.getStudentById(stdId);
	}
	
	@GetMapping("getStudentByName/{name}")
	public Student getStudentByName(@PathVariable("name") String stdName) {
		return studentDao.getStudentByName(stdName);
	}
	
	@PostMapping("addStudent")
	public Student addStudent(@RequestBody Student student){
		return studentDao.addStudent(student);
	}
	@PutMapping("updateStudent")
	public  Student updateStudent(@RequestBody Student student){
		return studentDao.updateStudent(student);
	}
	
	@DeleteMapping("deleteStudentById/{id}")
	public String deleteStudentById(@PathVariable("id") int stdId){
		studentDao.deleteStudentById(stdId);
		return "Employee with EmployeeId: " +stdId+ ", Deleted Successfully";
	}
	
	@GetMapping("getLogin/{email}/{password}")
	public Student getLogin(@PathVariable("email") String emailId,@PathVariable("password") String password) {
		return studentDao.getLogin(emailId,password);
	}


	

}